from mertakdeniz.models import Config
from blog.models import Comment, Category, Tag


def global_params(request):
    return {
        'settings': Config.objects.first(),
        'latest_comments': Comment.objects.filter(status__exact=1)[:2],
        'categories': Category.objects.filter(parent__isnull=True),
        'tags': Tag.objects.all()
    }
