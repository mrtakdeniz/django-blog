# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-13 17:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_auto_20151213_1856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.CharField(max_length=150, unique=True),
        ),
    ]
