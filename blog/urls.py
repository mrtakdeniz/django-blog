from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.anasayfa, name='index'),
    url(r'^arama/$', views.results, name='search'),
    url(r'^anasayfa/sayfa/(?P<page_number>[0-9]+)/$', views.anasayfa, name='page'),
    url(r'^anasayfa/$', views.anasayfa, name='anasayfa'),
    url(r'^kategori/(?P<category_slug>[0-9A-Za-z\-]+)/$', views.category, name='category'),
    url(r'^etiket/(?P<tag_slug>[0-9A-Za-z\-]+)/$', views.tag, name='tag'),
    url(r'^(?P<article_slug>[0-9A-Za-z\-]+)/$', views.article, name='article'),
    url(r'yeni-yorum/(?P<article_slug>[0-9A-Za-z\-]+)/$', views.add_comment, name='new_comment'),
    url(r'yeni-yorum/(?P<article_slug>[0-9A-Za-z\-]+)/(?P<comment_id>[0-9A-Za-z\-]+)/$',
        views.add_comment, name='new_reply')
]