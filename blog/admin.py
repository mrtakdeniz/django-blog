from django.contrib import admin
from django import forms
from redactor.widgets import RedactorEditor
from blog.models import Article, Category, Comment, Tag


class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('title', 'slug', 'description', 'content', 'category', 'tags')
        widgets = {
            'content': RedactorEditor(),
            'description': RedactorEditor(),
        }


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    search_fields = ['content', 'title', 'slug']
    list_display = ('title', 'category', 'created_at', 'updated_at')
    filter_horizontal = ['tags']


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name', 'slug')


class CommentAdmin(admin.ModelAdmin):
    search_fields = ['owner_name', 'text', 'email', 'article']
    list_display = ('owner_name', 'text', 'email', 'article', 'status')

admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Tag)
# Register your models here.
