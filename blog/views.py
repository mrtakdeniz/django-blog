from blog.models import Category, Article, Tag, Comment
from django.shortcuts import get_object_or_404, render, get_list_or_404, redirect
from django.db.models import Q


def index(request):
    return render(request, 'blog/rest.html')


def anasayfa(request):
    return render(request, 'blog/home.html', {
        'page_title': 'Anasayfa',
        'articles': Article.objects.all()
    })


def article(request, article_slug):
    return render(request, 'blog/detail.html', {
        'article': get_object_or_404(Article, slug=article_slug),
        'article_comments': Comment.objects
                  .filter(article__slug__exact=article_slug)
                  .filter(parent__isnull=True)
                  .filter(status__exact=True)})


def category(request, category_slug):
    current_category = get_object_or_404(Category, slug=category_slug)
    return render(request, 'blog/home.html', {
        'page_title': current_category.name,
        'articles': Article.objects.filter(category__slug__exact=category_slug),
        'current_category': current_category
    })


def tag(request, tag_slug):
    current_tag = get_object_or_404(Tag, slug=tag_slug)
    return render(request, 'blog/home.html', {
        'page_title': current_tag.name,
        'articles': Article.objects.filter(tags__slug__exact=tag_slug),
        'current_tag': current_tag
    })


def results(request):
    if request.method == "POST":
        keyword = request.POST['keyword']
        result = Article.objects.filter(Q(title__icontains=keyword) |
                                        Q(content__icontains=keyword) |
                                        Q(description__icontains=keyword) |
                                        Q(tags__name__icontains=keyword)).distinct()
        return render(request, 'blog/home.html', {
            'page_title': "%s için sonuçlar" % keyword,
            'articles': result
        })
    else:
        return render(request, 'blog/home.html', {
            'page_title': 'anasayfa',
            'articles': Article.objects.all()
        })


def add_comment(request, article_slug, comment_id=None):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        message = request.POST['message']

        current_article = get_object_or_404(Article, slug=article_slug)

        if comment_id is not None:
            current_comment = get_object_or_404(Comment, id=comment_id)
            new_comment = Comment(owner_name=name,
                                  email=email,
                                  text=message,
                                  article=current_article,
                                  parent=current_comment)
        else:
            new_comment = Comment(owner_name=name,
                                  email=email,
                                  text=message,
                                  article=current_article)

        new_comment.save()

        return redirect('article', article_slug=article_slug, permanent=True)
