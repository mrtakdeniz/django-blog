from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=150, unique=True, verbose_name="Kategori")
    slug = models.CharField(max_length=150, unique=True, verbose_name="Slug")
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True, verbose_name="Oluşturulma")
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True, verbose_name="Düzenlenme")
    parent = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        verbose_name = "Kategori"
        verbose_name_plural = "Kategoriler"
        ordering = ['name']

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name="Etiket")
    slug = models.CharField(max_length=100, verbose_name="Slug", null=True, blank=True)

    class Meta:
        verbose_name = "Etiket"
        verbose_name_plural = "Etiketler"

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=150, unique=True, verbose_name="Başlık")
    slug = models.CharField(max_length=150, verbose_name="Slug", unique=True)
    description = models.TextField(verbose_name="Açıklama", null=True, blank=True)
    content = models.TextField(verbose_name="İçerik")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name="Kategori")
    tags = models.ManyToManyField(Tag, blank=True)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True, verbose_name="Oluşturulma")
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True, verbose_name="Düzenlenme")

    class Meta:
        verbose_name = "Makale"
        verbose_name_plural = "Makaleler"
        ordering = ['-created_at']

    def __str__(self):
        return self.title

    def get_active_comment_count(self):
        return self.comment_set.filter(status__exact=True).count()


class Comment(models.Model):
    owner_name = models.CharField(max_length=150, verbose_name="Ad-Soyad")
    email = models.CharField(max_length=150, verbose_name="E-Posta")
    text = models.TextField(verbose_name="Yorum")
    article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Makale")
    parent = models.ForeignKey('self', null=True, blank=True)
    status = models.BooleanField(default=0, verbose_name="Durum")
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True, verbose_name="Oluşturulma")
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True, verbose_name="Düzenlenme")

    class Meta:
        verbose_name = "Yorum"
        verbose_name_plural = "Yorumlar"
        ordering = ['-created_at']

    def __str__(self):
        return "%s - %s" % (self.owner_name, self.created_at)
