from django.contrib import admin
from .models import Config, Global

admin.site.register(Config)
admin.site.register(Global)
# Register your models here.
