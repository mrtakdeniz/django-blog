from mertakdeniz.models import Global, Config


def settings(request):
    setting = Config.objects.first()

    return {
        'settings': setting,
    }
