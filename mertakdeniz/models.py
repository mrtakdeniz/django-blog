from django.db import models


class Config(models.Model):
    site_name = models.CharField(max_length=150)
    site_version = models.FloatField()

    def __str__(self):
        return self.site_name


class Global(models.Model):
    facebook = models.CharField(max_length=150)
    twitter = models.CharField(max_length=150)

    def __str__(self):
        return self.facebook

# Create your models here.
